**Задание на ЛР:**

1. Разработать OpenMP-программу, выполняющую поэлементное и постолбцовое вычисление произведения квадратных матриц в разделяемой памяти. Сравнить время выполнения двух версий программ в зависимости от размера матрицы и количества потоков.
1. Разработать OpenMP-программу, выполняющую транспонирование квадратной матрицы на произвольном количестве потоков. Произвести экспериментальное сравнение времени выполнения двух версий программ в зависимости от размера матрицы и количества потоков.
1. Разработать последовательную и параллельную реализацию программы (с использованием OpenMP), демонстрирующую выполнение арифметических операций над векторами. Произвести экспериментальное сравнение времени выполнения двух версий программ в зависимости от размера матрицы и количества потоков.

**Дополнительная информация**

Для сборки OpenMP-программ использовать флаг -fopenmp :

Примеры:
1. gcc test.c -fopenmp -o test
1. mpicc mpitest.c -fopenmp -o mpitest
